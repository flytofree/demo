package com.loewi_xi.demo.logs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ErrorFileLog implements Log {

    private static Logger logger = LoggerFactory.getLogger(ErrorFileLog.class);

    @Override
    public void debug() {
        logger.debug("ErrorFileLog show log level: debug");
    }

    @Override
    public void info() {
        logger.info("ErrorFileLog show log level: info");
    }

    @Override
    public void warn() {
        logger.warn("ErrorFileLog show log level: warn");
    }

    @Override
    public void error() {
        logger.error("ErrorFileLog show log level: error");
    }
}
