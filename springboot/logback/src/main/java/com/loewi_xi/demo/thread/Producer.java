package com.loewi_xi.demo.thread;

public class Producer extends Thread {

    private int num;
    public AbstractStorage abstractStorage;

    public Producer(AbstractStorage abstractStorage) {
        this.abstractStorage = abstractStorage;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public void produce(int num) {
        abstractStorage.product(num);
    }

    @Override
    public void run() {
        produce(num);
    }
}
