package com.loewi_xi.demo.logs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WarnFileLog implements Log {

    private static Logger logger = LoggerFactory.getLogger(WarnFileLog.class);

    @Override
    public void debug() {
        logger.debug("WarnFileLog show log level: debug");
    }

    @Override
    public void info() {
        logger.info("WarnFileLog show log level: info");
    }

    @Override
    public void warn() {
        logger.warn("WarnFileLog show log level: warn");
    }

    @Override
    public void error() {
        logger.error("WarnFileLog show log level: error");
    }
}
