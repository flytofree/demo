package com.loewi_xi.demo;

import com.loewi_xi.demo.logs.ConsoleLog;
import com.loewi_xi.demo.logs.ErrorFileLog;
import com.loewi_xi.demo.logs.Log;
import com.loewi_xi.demo.logs.WarnFileLog;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
		List<Log> logs = Arrays.asList(new ConsoleLog(), new WarnFileLog(), new ErrorFileLog());
		logs.forEach(log -> {
			log.debug();
			log.info();
			log.warn();
			log.error();
		});
	}
}
