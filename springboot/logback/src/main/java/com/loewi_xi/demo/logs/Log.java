package com.loewi_xi.demo.logs;

public interface Log {

    /**
     * 输出debug日志
     */
    void debug();

    /**
     * 输出info日志
     */
    void info();

    /**
     * 输出warn日志
     */
    void warn();

    /**
     * 输出error日志
     */
    void error();
}
