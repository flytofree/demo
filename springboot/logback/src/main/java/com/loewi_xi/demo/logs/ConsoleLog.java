package com.loewi_xi.demo.logs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConsoleLog implements Log {

    private static Logger logger = LoggerFactory.getLogger(ConsoleLog.class);

    @Override
    public void debug() {
        logger.debug("ConsoleLog show log level: debug");
    }

    @Override
    public void info() {
        logger.info("ConsoleLog show log level: info");
    }

    @Override
    public void warn() {
        logger.warn("ConsoleLog show log level: warn");
    }

    @Override
    public void error() {
        logger.error("ConsoleLog show log level: error");
    }
}
