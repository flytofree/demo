package com.loewi_xi.demo.thread;

public interface AbstractStorage {

    void consume(int num);

    void product(int num);
}
