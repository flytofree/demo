## Spring 事务

#### 事务传播机制

首先，需要强调的一点是：事务的传播行为是针对 **嵌套事务** 而言的。其有 7种传播机制。在Spring Boot代码中，通过在 `@Transactional` 中的 **propagation** 来指定事务的传播机制，例如：
```java
@Transactional(propagation = Propagation.REQUIRED)
```

##### `Propagation.REQUIRED`

支持事务，如果业务方法执行时已经在一个事务中，那么方法加入当前事务继续执行，否则就新启一个事务执行。是Spring默认的事务传播机制。

对于内层事务来说，只有外层事务提交了，内层事务才会提交。如果内层事务或外层事务中任意一个有报错，那么它俩会一起回滚，即使外层方法通过 `try-catch` 方法来处理内层方法报错抛出的异常。这是因为内外层方法处在同一个事务中，任意一处抛出异常，整个事务就被设置成 **rollback-only**，所以不管之后怎么处理，该事务都会回滚。

例如（内层方法抛出异常，外层方法通过 `try-catch` 处理内层方法的异常）：

```java
// 外层方法
@Service
public class UserService {
    @Resource
    private UserMapper userMapper;
    @Autowired
    private StudentService studentService;
 
    @Transactional
    public int addUser(User user) {
        int i = userMapper.insertSelective(user);
        try {
            Student student = new Student();
            student.setCourse("cs");
            student.setName("sid");
            studentService.addStudent(student);
        }catch (Exception ignored){
            // 异常不再抛出
        }
        return i;
    }
}

// 内层方法
@Service
public class StudentService {
    @Resource
    private StudentMapper studentMapper;
 
    @Transactional(propagation = Propagation.REQUIRED)
    public int addStudent(Student student) {
        int i = studentMapper.insertSelective(student);
        int j = 10 / 0; // 抛出异常
        return i;
    }
}
```

上述代码的执行结果是：**事务发生回滚，user表和student表插入数据没有成功**。

##### `Propagation.REQUIRES_NEW`

支持事务，业务方法总是会创建一个新事务执行，如果业务方法已经在一个事务中，那么会挂起当前事务。

内层事务执行结束，会立即提交，不会去等待外层事务的执行情况，即使外层事务抛出异常进行回滚，也不会有影响。同样，如果内层事务抛出异常进行回滚，而外层方法通过 `try-catch` 处理异常，那么外层事务会正常提交而不受内层事务影响，除非也有异常抛出。

例如：
- （内层方法执行无异常，外层方法抛出异常）：

```java
// 外层方法
@Service
public class UserService {
    @Resource
    private UserMapper userMapper;
    @Autowired
    private StudentService studentService;
    
    @Transactional
    public int addUser(User user) {
        int i = userMapper.insertSelective(user);
        Student student = new Student();
        student.setCourse("cs");
        student.setName("sid");
        studentService.addStudent(student);
        int j = 10 / 0; // 抛出异常
        return i;
    }
}

// 内层方法
@Service
public class StudentService {
    @Resource
    private StudentMapper studentMapper;
    
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public int addStudent(Student student) {
        return studentMapper.insertSelective(student);
    }
}
```

上述代码的执行结果是：**student表插入数据成功，而user表插入数据没有成功**。

- （内层方法抛出异常，外层方法执行无异常但未处理内层方法抛出的异常）：

```java
// 外层方法
@Service
public class UserService {
    @Resource
    private UserMapper userMapper;
    @Autowired
    private StudentService studentService;

    @Transactional
    public int addUser(User user) {
        int i = userMapper.insertSelective(user);
        Student student = new Student();
        student.setCourse("cs");
        student.setName("sid");
        studentService.addStudent(student);
        return i;
    }
}

// 内层方法
@Service
public class StudentService {
    @Resource
    private StudentMapper studentMapper;
 
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public int addStudent(Student student) {
        int i = studentMapper.insertSelective(student);
        int j = 10 / 0; // 抛出异常
        return i;
    }
}
```

上述代码的执行结果是：**student表插入数据没有成功，同样，user表插入数据也没有成功**。

##### `Propagation.NESTED`

支持事务，如果业务方法执行时已经在一个事务中，那么方法嵌套在已有的事务中，并作为一个子事务执行，否则就新启一个事务执行。

内/外层事务的执行与 `Propagation.REQUIRED` 相似：内层事务要等外层事务执行完成后一起提交；外层回滚时，内层也一起回滚。

但不同的是：内层回滚时，外层不会受到影响。但这有一些前提条件：
- 需要 `java.sql.Savepoint` 的支持，即 JDK版本需要在 1.4以上；
- 事务管理器的 `nestedTransactionAllowed` 的属性值需要为 `true`；
- 内层抛出的异常，外层需要 `try-catch` 处理，且外层能不抛出异常顺利执行。

例如：
- （内层方法执行无异常，外层方法抛出异常）：

```java
// 外层方法
@Service
public class UserService {
    @Resource
    private UserMapper userMapper;
    @Autowired
    private StudentService studentService;

    @Transactional
    public int addUser(User user) {
        int i = userMapper.insertSelective(user);
        Student student = new Student();
        student.setCourse("cs");
        student.setName("sid");
        studentService.addStudent(student);
        int i1 = 10 / 0; // 抛出异常
        return i;
    }
}

// 内层方法
@Service
public class StudentService {
    @Resource
    private StudentMapper studentMapper;

    @Transactional(propagation = Propagation.NESTED)
    public int addStudent(Student student) {
        return studentMapper.insertSelective(student);
    }
}
```

上述代码的执行结果是：**事务发生回滚，user表和student表插入数据没有成功**。

- （内层方法抛出异常，外层方法通过 `try-catch` 处理内层方法的异常）：

```java
// 外层方法
@Service
public class UserService {
    @Resource
    private UserMapper userMapper;
    @Autowired
    private StudentService studentService;

    @Transactional
    public int addUser(User user) {
        int i = userMapper.insertSelective(user);
        try {
            Student student = new Student();
            student.setCourse("cs");
            student.setName("sid");
            studentService.addStudent(student); 
        }catch (Exception ignored){
            // 异常不再抛出
        }
        return i;
    }
}

// 内层方法
@Service
public class StudentService {
    @Resource
    private StudentMapper studentMapper;
 
    @Transactional(propagation = Propagation.NESTED)
    public int addStudent(Student student) {
        int i = studentMapper.insertSelective(student);
        int j = 10 / 0; // 抛出异常
        return i;
    }
}
```

上述代码的执行结果是：**student表插入数据没有成功，而user表插入数据成功**。

##### `Propagation.SUPPORTS`

支持事务，如果业务方法执行时已经在一个事务中，那么方法加入当前事务执行，否则就以非事务方式执行，不会新启一个事务执行。

例如：
- （无事务调用）：

```java
@Service
public class StudentService {
    @Resource
    private StudentMapper studentMapper;

    @Transactional(propagation = Propagation.SUPPORTS)
    public int addStudent(Student student) {
        return studentMapper.insertSelective(student);
    }
}
```

上述代码的执行结果是：**方法无事务执行，student表插入数据成功**。

- （有事务调用）：
2.调用addStudent的外层方法有事务

```java
// 外层方法
@Service
public class UserService {
    @Resource
    private UserMapper userMapper;
    @Autowired
    private StudentService studentService;

    @Transactional
    public int addUser(User user) {
        int i = userMapper.insertSelective(user);
        Student student = new Student();
        student.setCourse("cs");
        student.setName("sid");
        studentService.addStudent(student);
        return i;
    }
}

// 内层方法
@Service
public class StudentService {
    @Resource
    private StudentMapper studentMapper;

    @Transactional(propagation = Propagation.SUPPORTS) 
    public int addStudent(Student student) {
        return studentMapper.insertSelective(student);
    }
}
```

上述代码的执行结果是：**所有方法执行完成后，student表插入数据才成功**。

##### `Propagation.MANDATORY`

支持事务，如果业务方法执行时已经在一个事务中，那么方法加入当前事务执行，否则抛出异常。

例如：
- （无事务调用）：

```java
@Service
public class StudentService {
    @Resource
    private StudentMapper studentMapper;

    @Transactional(propagation = Propagation.MANDATORY)
    public int addStudent(Student student) {
        return studentMapper.insertSelective(student);
    }
}
```

上述代码的执行结果是：**抛出 `IllegalTransactionStateException` 异常**。

- （有事务调用）：

```java
// 外层方法
@Service
public class UserService {
    @Resource
    private UserMapper userMapper;
    @Autowired
    private StudentService studentService;

    @Transactional
    public int addUser(User user) {
        int i = userMapper.insertSelective(user);
        Student student = new Student();
        student.setCourse("cs");
        student.setName("sid");
        studentService.addStudent(student);
        return i;
    }
}

// 内层方法
@Service
public class StudentService {
    @Resource
    private StudentMapper studentMapper;

    @Transactional(propagation = Propagation.MANDATORY)
    public int addStudent(Student student) {
        return studentMapper.insertSelective(student);
    }
}
```

上述代码的执行结果是：**所有方法执行完成后，student表插入数据才成功**。

##### `Propagation.NOT_SUPPORTED`

不支持事务，如果业务方法执行时已经在一个事务中，那么挂起当前事务，等方法执行完毕后，事务恢复并继续进行。

例如（有事务调用）：

```java
// 外层方法
@Service
public class UserService {
    @Resource
    private UserMapper userMapper;
    @Autowired
    private StudentService studentService;

    @Transactional
    public int addUser(User user) {
        int i = userMapper.insertSelective(user);
        Student student = new Student();
        student.setCourse("cs");
        student.setName("sid");
        studentService.addStudent(student);
        return i;
    }
}

// 内层方法
@Service
public class StudentService {
    @Resource
    private StudentMapper studentMapper;

    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public int addStudent(Student student) {
        return studentMapper.insertSelective(student);
    }
}
```

上述代码的执行结果是：**student表在 `addStudent` 方法执行完之后数据就插入成功，而user表数据需要在所有方法执行完之后数据才插入成功**。

##### `Propagation.NEVER`

不支持事务，如果当前方法执行时已经在一个事务中，则抛出异常。

例如（有事务调用）：

```java
// 外层方法
@Service
public class UserService {
    @Resource
    private UserMapper userMapper;
    @Autowired
    private StudentService studentService;

    @Transactional
    public int addUser(User user) {
        int i = userMapper.insertSelective(user);
        Student student = new Student();
        student.setCourse("cs");
        student.setName("sid");
        studentService.addStudent(student);
        return i;
    }
}

// 内层方法
@Service
public class StudentService {
    @Resource
    private StudentMapper studentMapper;

    @Transactional(propagation = Propagation.NEVER)
    public int addStudent(Student student) {
        return studentMapper.insertSelective(student);
    }
}
```

上述代码的执行结果是：**抛出 `IllegalTransactionStateException` 异常**。

#### 事务的隔离机制

```java
@Transactional(isolation = Isolation.DEFAULT)
```

- `Isolation.DEFAULT`：Spring默认的隔离级别，表示使用数据库默认的事务隔离级别；
- `Isolation.READ_UNCOMMITTED`：事务最低的隔离级别，允许其它事务可以读取当前事务未提交的数据，会产生脏读，不可重复读和幻读；
- `Isolation.READ_COMMITTED`：保证一个事务修改的数据提交后，其它事务才可以读取，能避免脏读的产生，但会出现不可重复读和幻读；
- `Isolation.REPEATABLE_READ`：事务执行期间，不管其它事务对数据进行任何修改，当前事务读取数据的值仍和其启动时相同，能避免脏读和不可重复读，但会出现幻读；
- `Isolation.SERIALIZABLE`：所有事务按顺序执行，能避免脏读，不可重复读和幻读。

> PS:
> 
> - 脏读：当一个事务访问数据并对数据进行修改，但其修改还没提交到数据库时，另一个事务访问这个数据并进行使用，而先前的事务出现异常进行了回滚。举例来说：
>> Mary的原工资为 1000元，财务人员将 Mary的工资修改为 8000元，但还未进行提交。此时，Mary读取自己的工资，发现自己的工资变为了 8000元，欢天喜地（脏读）！但财务发现操作有误，回滚了事务，Mary的工资又变为了 1000元。
>
> - 不可重复读：在一个事务中，前后两次读取的结果并不一致。举例来说：
>> 在事务 1中，Mary读取了自己的工资为 1000元，但操作还没有完成。在事务 2中，财务人员修改了 Mary的工资为 2000元，并提交了事务。回到事务 1中，Mary再次读取自己的工资时，发现工资变为了 2000元。
>
> - 幻读：第一个事务对表中的全部数据行进行修改。同时，第二个事务向表中新插入一行数据。那么操作第一个事务的用户会发现表中还有未修改的数据。举例来说：
>> 目前工资为 1000元的员工有10人。事务 1，读取所有工资为 1000元的员工。这时事务 2向employee表插入了一条新员工记录，工资也为 1000元。因此，事务 1再次读取所有工资为 1000元的员工，共读取到了11条记录。
