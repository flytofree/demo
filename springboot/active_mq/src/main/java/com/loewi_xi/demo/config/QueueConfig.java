package com.loewi_xi.demo.config;

import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.jms.Queue;

@Component
public class QueueConfig {

    @Value("${destination}")
    private String destination;

    @Bean
    public Queue getQueue() {
        return new ActiveMQQueue(destination);
    }
}
