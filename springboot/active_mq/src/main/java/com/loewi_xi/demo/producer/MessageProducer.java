package com.loewi_xi.demo.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.jms.Queue;

@Component
public class MessageProducer {

    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;
    @Autowired
    private Queue queue;

    //每隔5s时间向队列发送消息
    @Scheduled(fixedDelay = 5000)
    public void send() {
        System.out.println(1234);
        String msgString = System.currentTimeMillis() + " ";
        jmsMessagingTemplate.convertAndSend(queue, msgString);
        System.out.println("点对点通讯，msg" + msgString);
    }
}
